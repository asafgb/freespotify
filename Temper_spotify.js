// ==UserScript==
// @name         Spotify_ADs_blocker
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://open.spotify.com/*
// @grant        none
// ==/UserScript==

(function() {
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function CheckIfAds() {

    var volumebar=document.getElementsByClassName("volume-bar")[0]
    while (true) {
        var i;
        var allLinks = document.getElementsByClassName("now-playing-bar")[0].getElementsByTagName("a")
        var foundAds = false;

        for (i = 0; i < allLinks.length; i++) {
            if (!(allLinks[i].href.includes("https://open.spotify.com") || allLinks[i].href.includes("https://www.spotify.com"))) {
                foundAds = true
            }
        }

        var mutebutton =volumebar.getElementsByTagName("button")[0]
        if (foundAds) {
            if(mutebutton.getAttribute("aria-label")!=null && mutebutton.getAttribute("aria-label")=="Mute")
            {
                mutebutton.click()
            }
        }
        else {
            if(mutebutton.getAttribute("aria-label")!=null && mutebutton.getAttribute("aria-label")=="Unmute")
            {
                mutebutton.click()
            }

        }

        await sleep (2000)
    }
}


setTimeout(function () {
    CheckIfAds()

}, 2000);
})();
